from SIP import app,lm

from flask import request, session, g, redirect, url_for, abort, render_template, flash,g
from flask.ext.login import login_user, logout_user, current_user, login_required
#importing forms
from forms import ContactForm,SignupForm,SigninForm,VerificationForm,ResetPassword,ResetPasswordSubmit,SubmitExperience,AddComment,AdminVerification
from flask.ext.mail import Message, Mail
from models import db,User,Verified,Posts,Company,Comments
from itsdangerous import JSONWebSignatureSerializer
from werkzeug import generate_password_hash
from datetime import datetime
mail = Mail()

@app.before_request
def before_request():
    g.user=current_user     #current_user global is set by Flask-Login


@lm.user_loader     #loads a user from database
def load_user(id):
    return User.query.get(int(id))
    
    
@app.route('/')
def home():
  posts = Posts.query.order_by(Posts.timeposted.desc())
  companies = Company.query.all()
  postdetails = []
  for post in posts:
      if post.verificationbit != 0:	#display verified posts only
	  company = Company.query.filter_by(companyid = post.companyid).first()
	  user=User.query.get(post.userid)
	  postdetails.append([user,post,company])
	  
  return render_template('home.html',
		  posts =  postdetails,
		  companies = companies)


@app.route('/comments/<int:postid>',methods = ['GET','POST'])
@login_required
def comments(postid):
   if 'email' not in session:
	flash("You need to sign in to comment")
        return redirect(url_for('home'))
   #posts=Posts.query.filter_by(postid=postid)
   if request.method == 'POST':
      form = AddComment()
      comment = Comments(postid,g.user.user_id,form.comment.data)
      db.session.add(comment)
      db.session.commit()
      flash('Successfully posted')
      return redirect('/posts/'+str(postid))
   elif request.method=='GET':
      return redirect('/posts/'+str(postid))

@app.route('/posts/<int:postid>')
def viewPost(postid):
    post = Posts.query.filter_by(postid = postid).first()
    company = Company.query.filter_by(companyid = post.companyid).first()
    companies = Company.query.all()
    user = User.query.filter_by(user_id = post.userid).first()
    commentdetails = []
    comments = Comments.query.filter_by(postid = postid)
    for comment in comments: #attach author of comment to each comment
	  commentuser = User.query.get(comment.userid)
	  commentdetails.append([comment,commentuser])
    return render_template('post.html',post = post,
		    company = company,
		    user = user,
		    companies = companies,
		    comments = commentdetails)

@app.route('/posts/<path:path>')
def viewCompanyPosts(path):
    company = Company.query.filter_by(companyname = path).first()
    postdetails = []
    if company is None:
        return render_template('404.html'),404
    posts = Posts.query.filter_by(companyid = company.companyid)
    for post in posts:
        user=User.query.get(post.userid)
        postdetails.append([user,post, company])
    companies = Company.query.all()
    return render_template('companyposts.html',posts = postdetails,companies = companies,company = company)

@app.route('/about')
def about():
  return render_template('about.html')
  
@app.route('/contact', methods=['GET', 'POST'])
def contact():
  form = ContactForm()
  if request.method == 'POST':
    if form.validate() == False:
      flash('All fields are required.')
      return render_template('contact.html', form=form)
    else:
      msg = Message(form.subject.data, sender='shuvham683bitsian@gmail.com', recipients=['f2011683@hyderabad.bits-pilani.ac.in'])
      msg.body = """
      From: %s <%s>
      %s
      """ % (form.name.data, form.email.data, form.message.data)
      mail.send(msg)
 
      return render_template('contact.html', success=True)
 
  elif request.method == 'GET':
    return render_template('contact.html', form=form)

@app.route('/verify/<email>', methods=['GET', 'POST'])
def verify(email):
    form=VerificationForm()
    if 'email' in session:
        return redirect(url_for('profile'))
    if request.method=='POST':
        x=form.validate(email)
        print 'x= '+str(x)
        if x == 1:
            return render_template('verify.html', form=form,email=email)
        elif x==2 or x==4:
            flash('No such User')
            return redirect(url_for('home'))
        elif x==3:
            flash('Already verified. please signin to continue')
            return redirect(url_for('home'))
        elif x==0:
            user=User.query.filter_by(email=email).first()
            session['email']=email
            user.verified=1
            
            db.session.add(user)
            db.session.commit()
            remember_me=session['email']
            login_user(user, remember = remember_me)
            return redirect(url_for('profile'))
    
    elif request.method=='GET':
        user1=User.query.filter_by(email=email).first()
        if user1 and user1.verified==0:
            return render_template('verify.html', form=form,email=email)
        else:
            flash('Already verified. please signin to continue')
            return redirect(url_for('home'))
    
    
    
@app.route('/signup', methods=['GET', 'POST'])

def signup():
  form = SignupForm()
  if 'email' in session:
    return redirect(url_for('profile')) 
  if request.method == 'POST':
    if form.validate() == False:
      return render_template('signup.html', form=form)
    else:
      
      newuser=User(form.firstname.data,form.lastname.data,form.email.data,form.password.data)
      db.session.add(newuser)
      db.session.commit()
     
      user=User.query.filter_by(email=form.email.data).first()
      s= JSONWebSignatureSerializer('secret-key')
      a=s.dumps({'x':user.user_id})
      token=a[len(a)-15:]
      msg = Message("One time password", sender='shuvham683bitsian@gmail.com', recipients=[form.email.data+'@hyderabad.bits-pilani.ac.in'])
      msg.body = """
      From: %s <%s>
      %s
      """ % ('Admin@SIP', 'YOUR OTP TO COMPLETE SIGNUP', 'Your one time password is: '+token )
      mail.send(msg)
      verify=Verified(user.user_id,token)
      db.session.add(verify)
      db.session.commit()
      return redirect(url_for('verify',email=form.email.data))
    
  elif request.method == 'GET':
    return render_template('signup.html', form=form)
    
    

@app.route('/profile')
@app.route('/profile/<int:page>')
@login_required
def profile(page = 1):
 
  if 'email' not in session:
    return redirect(url_for('signin'))
 
  user = User.query.filter_by(email = session['email']).first()
  
  if user is None:
    return redirect(url_for('signin'))
  else:
    posts = Posts.query.filter_by(userid = g.user.user_id).order_by(Posts.timeposted.desc())
    companies = Company.query.all()
    postdetails = []
    for post in posts:
        company = Company.query.get(post.companyid)
	user = g.user
	postdetails.append([user,post,company])
    #postdetails.paginate(page, 2, False)
  return render_template('profile.html', posts =  postdetails,  companies = companies)

@app.route('/signin', methods=['GET', 'POST'])
def signin():
  form = SigninForm()
  if 'email' in session:
    return redirect(url_for('profile')) 
  if request.method == 'POST':
    if form.validate() == False:
      return render_template('signin.html', form=form)
    else:
      
      temp=form.email_in.data
      temp=temp.split('@')[0]
      user=User.query.filter_by(email=temp).first()
      if (user.verified==1):
          session['email'] = temp
          remember_me=session['email']
          login_user(user, remember = remember_me)
          flash('You are logged in')
          return redirect(url_for('profile'))
      else:
          flash('You are not a verified user.Please enter the OTP sent to you')
          return redirect(url_for('verify',email=user.email))      
                 
  elif request.method == 'GET':
    return render_template('signin.html', form=form)
    
    
@app.route('/signout')
@login_required
def signout():
 
  if 'email' not in session:
    return redirect(url_for('signin'))
     
  session.pop('email', None)
  logout_user()
  return redirect(url_for('home'))
  
@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500  
    
@app.route('/verified_change/<useremail>',methods=('GET', 'POST',)) 
def verified_change(useremail):
    if ('email' in session):# or verified_token==False:
        return redirect(url_for('home')) 
    password_change=ResetPasswordSubmit()
    if request.method == 'POST':
         if password_change.validate() == False:
             
              return render_template('changepassword.html',form=password_change,useremail=useremail )          
           
         else:
              user=User.query.filter_by(email=useremail).first()
              
              user.pwdhash = generate_password_hash(password_change.password.data)
              user.changepwd=0
              db.session.add(user)
              db.session.commit()
              #return "password updated successfully"
              flash("password updated successfully. Please Login to continue")
              return redirect(url_for('signin'))
                
    elif request.method=='GET':
        user=User.query.filter_by(email=useremail).first()
        if user and user.changepwd==1:
            return render_template('changepassword.html',form=password_change,useremail=useremail )     
        else:
            flash('Invalid access')    
            return redirect(url_for('signout'))
                      
@app.route('/change_password', methods=('GET', 'POST',))
def change_password():
    if 'email' in session:
        session.pop('email', None)
        logout_user() 
    token = request.args.get('token',None)
    if token:
        verified_result=User.verify_token(token)
        if verified_result:
            
            return redirect(url_for('verified_change',useremail=verified_result.email))
        else:
         #   verified_token=False
            flash('Invalid token')
            return redirect(url_for('signin'))
    else:
     #   verified_token=False
        flash('Invalid Access')
        return redirect(url_for('signin'))
                    

        
@app.route('/forget_password', methods=('GET', 'POST',))
def forget_password():
    if 'email' in session:
        return redirect(url_for('profile')) 
    
    form = ResetPassword() #form
    if request.method == 'POST':
        if form.validate() == False:
            return render_template('reset.html', form=form)
        else:
            temp=form.email_in.data
            temp=temp.split('@')[0]
            print temp
            user=User.query.filter_by(email=temp).first()
            if user:
                print temp
            user.changepwd=1
            db.session.add(user)
            db.session.commit()
            token = user.get_token()
            msg = Message("Password Reset", sender='shuvham683bitsian@gmail.com', recipients=[temp+'@hyderabad.bits-pilani.ac.in'])
            msg.body = """From: %s
                 <%s>  %s """ % ('Admin@SIP', 'PLEASE CLICK ON LINK TO SET YOUR PASSWORD', 'http://127.0.0.1:5000'+url_for('change_password',token=token)) 
            mail.send(msg)
            flash('Reset link has been sent to your account')
            
            return redirect(url_for('forget_password'))
            
    elif request.method == 'GET':
        return render_template('reset.html', form=form) 
        
        
@app.route('/addingExperience', methods=('GET', 'POST',))  
@app.route('/addingExperience/<int:edit>', methods=('GET', 'POST',)) 
@login_required
def addingExperience(edit=0):
    form=SubmitExperience()
    if request.method=='POST':
        compid=request.form.get('company')
        if not compid:
            flash('Select a Company')
            return render_template('addingExperiences.html',form=form,edit=edit)  
        if form.validate()==False:
            return render_template('addingExperiences.html',form=form,edit=edit)
        else:
            company=Company.query.filter_by(companyid=int(compid)).first()
            if (edit):
                post=Posts.query.filter_by(postid=int(edit)).first()
                post.jobprofile=form.jobprofile.data
                post.response=form.response.data
                post.tips=form.tips.data
                post.timeposted=datetime.utcnow()
                post.companyid=company.companyid
                db.session.add(post)
                db.session.commit()
                flash('Successfully updated')
                return redirect(url_for('profile'))
            else:
                post=Posts(g.user.user_id,company.companyid,datetime.utcnow(),form.jobprofile.data,form.response.data,form.tips.data)
                db.session.add(post)
                db.session.commit()
                flash('Successfully submitted for review')
                return redirect(url_for('profile'))
    elif request.method=='GET':
        return render_template('addingExperiences.html',form=form,edit=edit)
        
@app.route('/edit/<int:postid>')
@login_required
def edit(postid):
    post=Posts.query.filter_by(postid=postid).first()
    companyid=Company.query.filter(Company.companyid == post.companyid).first()
    form=SubmitExperience(obj=post)
    form.populate_obj(post)
    return render_template('addingExperiences.html', form=form,edit=postid,companyid=companyid)      

@app.route('/adminverification',methods = ('GET','POST',))        
@app.route('/adminverification/<int:postid>',methods = ('GET','POST',))
@login_required
def adminverification(postid=0):
    form=AdminVerification()
    if g.user.user_id == 0: #user_id 0 belongs to admin
        post = Posts.query.filter(Posts.verificationbit == 0).all()
        if request.method=='GET':
            return render_template('adminverification.html',form=form,posts=post)	  
        elif request.method=='POST':
              post = Posts.query.filter(Posts.postid == postid).first()
	      post.verificationbit=1
              db.session.add(post)
              db.session.commit()
              flash('Successfully verified')
              post = Posts.query.filter(Posts.verificationbit==0).all()
              return render_template('adminverification.html',form=form,posts=post)
	     
    else:
        flash('Not authorized')
        return redirect(url_for('home'))

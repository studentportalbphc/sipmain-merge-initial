from flask import Flask

from flask.ext.login import LoginManager
from momentjs import momentjs

app = Flask(__name__)
app.jinja_env.globals['momentjs'] = momentjs
app.secret_key = 'development key'
 
app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'shuvham683bitsian@gmail.com'
app.config["MAIL_PASSWORD"] = '9334835357'
 
lm = LoginManager()
lm.init_app(app)
lm.login_view='signin'

from routes import mail
mail.init_app(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:admin@localhost/sip'
 
from models import db
db.init_app(app)
import SIP.routes

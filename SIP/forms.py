from flask.ext.wtf import Form

from wtforms import TextField, BooleanField,TextAreaField,SubmitField,validators,ValidationError,PasswordField
from wtforms.validators import Required
from models import db,User,Verified
class ContactForm(Form):
  name = TextField("Name",  [validators.Required("Please enter your name.")])
  email_c = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter Valid email address.")])
  subject = TextField("Subject",  [validators.Required("Please enter a subject.")])
  message = TextAreaField("Message",  [validators.Required("Please enter a message.")])
  submit = SubmitField("Send")
  
  
class SignupForm(Form):
  firstname = TextField("First name",  [validators.Required("Please enter your first name.")])
  lastname = TextField("Last name",  [validators.Required("Please enter your last name.")])
  email = TextField("Email",  [validators.Required("Please enter correct data")])
  password = PasswordField('Password', [validators.Length(min=5),validators.Required("Please enter a password.")])
  submit = SubmitField("Create account")
 
  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)
 
  def validate(self):
    if not Form.validate(self):
      return False
     
    user = User.query.filter_by(email = self.email.data.lower()).first() # SELECT * FROM users WHERE email = self.email.data.lower() LIMIT 1
    if user:
      self.email.errors.append("That email is already taken")
      return False
    else:
      return True
      
      
class SigninForm(Form):
  email_in = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
  password = PasswordField('Password', [validators.Required("Please enter a password.")])
  submit = SubmitField("Sign In")
  
   
  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)
 
  def validate(self):
    if not Form.validate(self):
      return False
    temp=self.email_in.data.lower()
    temp=temp.split('@')[0]
    user = User.query.filter_by(email = temp).first()
    if user and user.check_password(self.password.data):
      return True
    else:
      self.email_in.errors.append("Invalid e-mail or password")
      return False
      
class VerificationForm(Form):
    verify=TextField("Enter your OTP",  [validators.Required("Please enter valid data.")])
    submit = SubmitField("Verify")
    
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
    
    def validate(self,email_id):
        if not Form.validate(self):
            return 1
        user=User.query.filter_by(email=email_id).first()
        if user:
            if user.verified == 0:
                temp=Verified.query.filter_by(id=user.user_id).first()
                if temp:
                    if temp.token==self.verify.data:
                        return 0
                    else:
                        self.verify.errors.append("Invalid OTP")
                        return 1
                else:
                    #self.verify.errors.append("You have already been verified")
                    return 2
            else:
                return 3
        else:
           return 4                             
    
class ExistingUser(object):
    def __init__(self, message="Email doesn't exists"):
        self.message = message

    def __call__(self, form, field):
        if not User.query.filter_by(email=field.data).first():
            raise ValidationError(self.message)


class ResetPassword(Form):
    email_in = TextField('Enter your email', [validators.Required("Please enter your email address."), validators.Email("Invalid email format.")])
    submit=SubmitField("Reset")
    
    def __init__(self, *args, **kwargs):
       Form.__init__(self, *args, **kwargs)
 
    def validate(self):
       if not Form.validate(self):
         return False
       temp=self.email_in.data.lower()
       temp=temp.split('@')[0]
       user = User.query.filter_by(email = temp).first()
       if user:
         return True
       else:
         self.email_in.errors.append("Invalid e-mail")
         return False

class ResetPasswordSubmit(Form):
    password = PasswordField('Password',  [validators.Length(min=5),validators.Required("Please enter valid password"),validators.EqualTo('confirm', message='Passwords must match')] )
    confirm = PasswordField('Confirm Password', [validators.Required("Please enter valid password")])  
    submit=SubmitField("Reset password")
    
class SubmitExperience(Form):
    jobprofile=TextField('Job Profile', [validators.Required("Please enter your Job Profile.")])
    response=TextAreaField("Experience",  [validators.Required("Please enter a message.")])
    tips=TextAreaField("Some tips")
    submit=SubmitField("Post")
    update=SubmitField("Update")
    
class AddComment(Form):
    comment=TextAreaField("Leave a comment")
    submit=SubmitField("Submit")
    
    
class AdminVerification():
    submit = SubmitField("Submit")    
    
    
    
    
    
    
    
    
    
    
    
    
    
          

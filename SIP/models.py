from flask.ext.sqlalchemy import SQLAlchemy
from SIP import app
from werkzeug import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
#import re #regular expression for user_id

db = SQLAlchemy()
 
class User(db.Model):
  __tablename__ = 'users'
  user_id = db.Column(db.Integer, primary_key = True)
  firstname = db.Column(db.String(100))
  lastname = db.Column(db.String(100))
  email = db.Column(db.String(120), unique=True)
  pwdhash = db.Column(db.String(120))
  verified = db.Column(db.Integer)
  changepwd=db.Column(db.Integer)
   
  def __init__(self, firstname, lastname, email, password):
    self.firstname = firstname.title()
    self.lastname = lastname.title()
    self.email = email.lower()
    self.set_password(password)
    self.verified=0
    self.changepwd=0
    #self.user_id = int(re.findall(r'\d+',email)[0][2:]) #store 11555 if email is f2011555@hyderabad.bits-pilani.ac.in
  
  def is_active(self):
    return True
  def get_id(self):
    return unicode(self.user_id)  # python 2   
  def is_authenticated(self):
        return True 
  def is_anonymous(self):
        return False           
  def set_password(self, password):
    self.pwdhash = generate_password_hash(password)
   
  def check_password(self, password):
    return check_password_hash(self.pwdhash, password)
    
  def get_token(self, expiration=1800):
    s = Serializer(app.config['SECRET_KEY'], expiration)
    return s.dumps({'user': self.user_id}).decode('utf-8')

  @staticmethod
  def verify_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
       data = s.loads(token)
    except:
       return None
    id = data.get('user')
    if id:
       return User.query.get(id)
    return None
  def added_response(self):
        return Posts.query.filter(Posts.userid == self.user_id).order_by(Posts.timeposted.desc()) 
    
class Verified(db.Model):
    __tablename__='verification'
    id=db.Column(db.Integer,primary_key=True)
    token=db.Column(db.String(15))
    
    def __init__(self,id,token):
        self.id=id
        self.set_token(token)
        
    def set_token(self,token):
        self.token=token
    
class Posts(db.Model):
    __tablename__='posts'        
    postid = db.Column(db.Integer, primary_key = True, autoincrement = True)
    userid = db.Column(db.Integer)
    companyid = db.Column(db.Integer)
    jobprofile = db.Column(db.String(100))
    response = db.Column(db.String(10000))
    tips = db.Column(db.String(10000))
    timeposted=db.Column(db.DateTime)
    verificationbit = db.Column(db.Integer,default = 0)

    def __init__(self,userid,companyid,timeposted,jobprofile,response,tips):
        self.userid=userid
        self.companyid=companyid
        self.jobprofile=jobprofile
        self.response=response
        self.tips=tips
        self.timeposted=timeposted

class Company(db.Model):
    __tablename__ = 'company'
    companyid = db.Column(db.Integer, primary_key = True, autoincrement = True)
    companyname = db.Column(db.String(100))

    def __init__(self, companyname):
        self.companyname = companyname
    
class Comments(db.Model):
	__tablename__ = 'comment'
	commentid = db.Column(db.Integer, primary_key = True, autoincrement = True)
	postid = db.Column(db.Integer)
    	userid = db.Column(db.Integer)
	comment = db.Column(db.String(500))

        def __init__(self,postid,userid,comment):
	    self.postid = postid
	    self.userid = userid
	    self.comment = comment
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

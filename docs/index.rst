.. SIP documentation master file, created by
   sphinx-quickstart on Mon Nov 24 23:59:48 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SIP's documentation!
===============================


.. toctree::
   :maxdepth: 2

Installation guide:
*******************
How to run SIP on your machine.

Open a terminal window.

Enter directory sipmain::

        $ cd sipmain


1.SETUP DATABASE
-----------------
You will need mysql for this. Login to root user of your mysql client.::

        $ mysql -u root -p

You will be logged in to the mysql prompt ::

        mysql> create database sip;
        mysql> use sip;

Similarly execute(copy and paste in above) all lines given in database.txt for creating the necessary tables.

Exit the mysql client ::
        mysql> exit


2.INSTALL PACKAGES
-------------------

Install required packages using the following command (You need an internet connection to download new packages)::

        $ pip install -r requirements.txt

3.RUN
------
Start your mysql database server ::

        $ sudo service mysql start

Make sure you are in sipmain directory.
Finally run the development server using::

        python run_sip.py

You will get the following on the terminal:
        
         * Running on http://0.0.0.0:5000/
         * Restarting with reloader

Right click and open link in Browser.

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


Pending:
=========
1.Add Experience - company names to be fetched from company table in db, not html

2. ~~ Update experience - company name to be set default ~~

3. ~~ Comments - add timestamp, order by timestamp ~~

4.About page

5.User role to be defined (for now, admin is the user with user_id = 0)

6. ~~ Verification OTP mail server to be set studentportal.bphc@gmail.com ~~

7.Admin verification-posts page html

8.Allow user to add new company in addExperience

9.Disable access to non-authors to edit other users' experiences(only html done)

10.'Tips' div styling and text-wrap

11.Order by time descending in /posts/<company_name>

12.Paginate

Features:
==========
1.Comments enabled

2.Admin verification posts link added on profile page of admin

3.Edit post link(html) only available to author of post

4.Search feature in sidebar

